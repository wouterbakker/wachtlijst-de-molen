@extends('adminlte::page')

@section('title', 'Wachtlijst nummer')

@section('content_header')
  <div style="text-align:center;">
    <h1></h1>
  </div>

@stop

@section('content')
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="box">
          <div class="box-body">
            <div style="text-align:center;">
              <h2> Je staat op dit moment op ingeschreven als bewoner. Je hoeft verder niks te doen. </h2>
              <br>
              <h2> Je inschrijfnummer is <b>{{ Auth::user()->id }}</b>.<br>
                Geef altijd het inschrijfnummer door als je reageert op een woning. </h2> <br>
                <h2> Wil je iets veranderen aan je inschrijving, klik dan in het menu op profiel.</h2>
            </div>
          </div>
          <!-- /.box -->
        </div>
      </div>
    </div>
  </div>
@stop

@section('css')

@stop
