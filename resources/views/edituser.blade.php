@extends('adminlte::page')

@section('title', 'Mijn Account - De Molen')

@section('content_header')
    <div style="text-align:center;">
        <h1>{{$user->name }}</h1>
    </div>

@stop

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                @if(Session::has('messageSucces'))
                    <div class="alert alert-success">
                        {{ Session::get('messageSucces') }}
                    </div>
                @endif
                @if(Session::has('messageError'))
                    <div class="alert alert-danger">
                        {{ Session::get('messageError') }}
                    </div>
                @endif
                <div>
                    <label>Inschrijfnummer: {{ $user->id }}</label>
                </div>
                <div>
                    <label>Plaats op de wachtlijst: {{ $waitingListNumber }}</label>
                </div>
                <form action="{{ route('AdminEditUser', ['id' => $user->id]) }}" method="post">
                    {{ csrf_field() }}
                    <div>
                        <label>Naam: </label>
                    </div>
                    <div class="input-group mb-3">
                        <input type="text" name="name" class="form-control {{ $errors->has('name') ? 'is-invalid' : '' }}" value="{{ $user->name }}">
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fas fa-user"></span>
                            </div>
                        </div>

                        @if ($errors->has('name'))
                            <div class="invalid-feedback">
                                <strong>{{ $errors->first('name') }}</strong>
                            </div>
                        @endif
                    </div>
                    <div>
                        <label>E-mail: </label>
                    </div>
                    <div class="input-group mb-3">
                        <input type="email" name="email" class="form-control {{ $errors->has('email') ? 'is-invalid' : '' }}" value="{{ $user->email }}"
                               placeholder="voornaam@demolen.me">
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fas fa-envelope"></span>
                            </div>
                        </div>
                        @if ($errors->has('email'))
                            <div class="invalid-feedback">
                                <strong>{{ $errors->first('email') }}</strong>
                            </div>
                        @endif
                    </div>

                    <div>
                        <label>Geboortedatum: </label>
                    </div>
                    <div class="input-group mb-3">
                        <input type="date" name="birthday" class="form-control {{ $errors->has('birthday') ? 'is-invalid' : '' }}" value="{{ $user->birthday }}"
                               placeholder="dd/mm/yyyy">
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fas fa-user"></span>
                            </div>
                        </div>

                        @if ($errors->has('birthday'))
                            <div class="invalid-feedback">
                                <strong>{{ $errors->first('birthday') }}</strong>
                            </div>
                        @endif
                    </div>
                    <div>
                        <label>{{ $user->name }} wil graag per mail informatie ontvangen over: </label>
                    </div>
                    <div class="input-group">
                        <label class="form-check-label"> <input type="checkbox" name="studio" @if ($user->wantStudioMail) checked @endif>Een studio</label>
                    </div>
                    <div class="input-group">
                        <label class="form-check-label"><input type="checkbox" name="tweeKamer" @if ($user->wantTweeKamerMail) checked @endif>2-kamer appartement </label>
                    </div>
                    <div class="input-group">
                        <label class="form-check-label"><input type="checkbox" name="drieKamer" @if ($user->wantDrieKamerMail) checked @endif>3-kamer appartement </label>
                    </div>
                    <div class="input-group">
                        <label class="form-check-label"><input type="checkbox" name="vierKamer" @if ($user->wantVierKamerMail) checked @endif>4-kamer appartement </label>
                    </div>
                    <div class="input-group">
                        <label class="form-check-label"><input type="checkbox" name="jongerenWoning" @if ($user->wantJongerenWoningMail) checked @endif>Jongeren woning (maximaal 23 jaar) </label>
                    </div>
                    <div style="margin-top:10px;">
                        <label>Motivatie: </label>
                    </div>
                    <div class="input-group mb-3">
                        <textarea rows="10" name="motivation" class="form-control {{ $errors->has('motivation') ? 'is-invalid' : '' }}" placeholder="">{{ $user->motivation }}</textarea>
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fas fa-info"></span>
                            </div>
                        </div>

                        @if ($errors->has('motivation'))
                            <div class="invalid-feedback">
                                <strong>{{ $errors->first('motivation') }}</strong>
                            </div>
                        @endif
                    </div>
                    <div style="margin-top:10px;">
                        @if ($user->occupant)
                            <label> {{$user->name}} is op dit moment ingeschrijven als bewoner. Vink onderstaand vinkje uit om geen bewoner meer te laten zijn.</label>
                        @else
                            <label> {{$user->name}} is op dit moment NIET ingeschrijven als bewoner. Vink onderstaand vinkje aan om hem/haar bewoner te maken.</label>
                        @endif
                    </div>

                    <div class="input-group">
                        <label class="form-check-label"> <input type="checkbox" name="occupant" @if ($user->occupant) checked @endif> is bewoner vinkje</label>
                    </div>
                    <div style="margin-top:10px;">
                        <label>Huisnummer indien bewoner: </label>
                    </div>
                    <div class="input-group mb-3">
                        <input type="text" name="houseNumber" class="form-control {{ $errors->has('houseNumber') ? 'is-invalid' : '' }}" value="{{ $user->houseNumber }}">
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fas fa-info"></span>
                            </div>
                        </div>

                        @if ($errors->has('houseNumber'))
                            <div class="invalid-feedback">
                                <strong>{{ $errors->first('houseNumber') }}</strong>
                            </div>
                        @endif
                    </div>

                    @can('editPermissions')
                        <div style="margin-top:10px;">
                            <label>Rollen: </label>
                        </div>
                        <div class="input-group">
                            <label class="form-check-label"> <input type="checkbox" name="waitingListViewer" @if ($user->hasRole('waitingListViewer')) checked @endif>Wachtlijst bekijken</label>
                        </div>
                        <div class="input-group">
                            <label class="form-check-label"><input type="checkbox" name="waitingListAdmin" @if ($user->hasRole('waitingListAdmin')) checked @endif>Wachtlijst-editor</label>
                        </div>
                        <div class="input-group">
                            <label class="form-check-label"><input type="checkbox" name="superAdmin" @if ($user->hasRole('superAdmin')) checked @endif>Super Admin</label>
                        </div>
                    @endcan
                    @can('editUser')
                        <button type="submit" class="btn btn-primary btn-block btn-flat">
                            Bijwerken
                        </button>
                    @endcan
                    @can('editWaitingList')
                </form>
                <form class="reset" action="{{ route('AdminResetWaitingList', ['id' => $user->id]) }}" method="post">
                    <div>
                        {{ csrf_field() }}
                        <div>
                            <label>Plaats onderaan de wachtlijst </label>
                        </div>
                        <p>
                            Door op onderstaande knop te klikken wordt {{$user->name}} onderaan de wachtwoord geplaatst.
                        </p>
                        <button type="submit" class="btn btn-primary btn-danger btn-block btn-flat">
                            Plaats onderaan de wachtlijst
                        </button>
                    </div>
                </form>
                @endcan
                @can('editUser')
                    <form class="delete" action="{{ route('AdminDeleteUser',['id' => $user->id]) }}" method="post">
                        <div>
                            {{ csrf_field() }}
                            <div>
                                <label>Uitschrijven </label>
                            </div>
                            <p>
                                Door op onderstaande knop te klikken worden al gegevens van {{$user->name}} verwijderd.
                            </p>
                            <button type="submit" class="btn btn-primary btn-danger btn-block btn-flat">
                                Verwijder alle gegevens
                            </button>
                        </div>
                    </form>
                @endcan
            </div>
        </div>
    </div>
@stop

@section('css')

@stop

@section('js')
    @stack('js')
    @yield('js')

    <script>
        $(".delete").on("submit", function(){
            return confirm("Weet je het heel zeker? Deze knop werkt echt, alle gegevens van {{ $user->name }} zullen worden verwijderd.");
        });
    </script>
    <script>
        $(".reset").on("submit", function(){
            return confirm("Weet je het heel zeker? Deze knop werkt echt. {{ $user->name }} zal onderaan de wachtlijst geplaatst worden.");
        });
    </script>


@endsection
