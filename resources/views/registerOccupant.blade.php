@extends('adminlte::master')

@section('adminlte_css')
    @yield('css')
    <link href="{{ asset('css/register.css') }}" rel="stylesheet" type="text/css" >
@stop

@section('classes_body', 'register-page')

@section('body')
    <div class="register-box">
        <div class="register-logo">
            <a href="{{ url(config('adminlte.dashboard_url', 'home')) }}">{!! config('adminlte.logo', '<b>Wachtlijst</b>de Molen') !!}</a>
        </div>
        <div class="card">
            <div class="card-body register-card-body">
                <p class="login-box-msg"> <b> Schrijf je nu is als bewoner. Je kan dit formulier alleen invullen als je nu al woont op de Molen of als je een van de zorgvragende bewoners ondersteund. </b></p>
                <form action="{{ url(config('adminlte.register_url', 'register')) }}" method="post">
                    {{ csrf_field() }}
                    <div>
                        <label>Naam: </label>
                    </div>
                    <div class="input-group mb-3">
                        <input type="text" name="name" class="form-control {{ $errors->has('name') ? 'is-invalid' : '' }}" value="{{ old('name') }}"
                               placeholder="Voornaam Achternaam">
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fas fa-user"></span>
                            </div>
                        </div>

                        @if ($errors->has('name'))
                            <div class="invalid-feedback">
                                <strong>{{ $errors->first('name') }}</strong>
                            </div>
                        @endif
                    </div>
                    <div>
                        <label>E-mail: </label>
                    </div>
                    <div class="input-group mb-3">
                        <input type="email" name="email" class="form-control {{ $errors->has('email') ? 'is-invalid' : '' }}" value="{{ old('email') }}"
                               placeholder="voornaam@demolen.me">
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fas fa-envelope"></span>
                            </div>
                        </div>
                        @if ($errors->has('email'))
                            <div class="invalid-feedback">
                                <strong>{{ $errors->first('email') }}</strong>
                            </div>
                        @endif
                    </div>
                    <div>
                        <label>Wachtwoord: </label>
                    </div>
                    <div class="input-group mb-3">
                        <input type="password" name="password" class="form-control {{ $errors->has('password') ? 'is-invalid' : '' }}"
                               placeholder="Wachtwoord">
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fas fa-lock"></span>
                            </div>
                        </div>
                        @if ($errors->has('password'))
                            <div class="invalid-feedback">
                                <strong>{{ $errors->first('password') }}</strong>
                            </div>
                        @endif
                    </div>
                    <div class="input-group mb-3">
                        <input type="password" name="password_confirmation" class="form-control {{ $errors->has('password_confirmation') ? 'is-invalid' : '' }}"
                               placeholder="Herhaal wachtwoord">
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fas fa-lock"></span>
                            </div>
                        </div>
                        @if ($errors->has('password_confirmation'))
                            <div class="invalid-feedback">
                                <strong>{{ $errors->first('password_confirmation') }}</strong>
                            </div>
                        @endif
                    </div>
                    <div>
                        <label>Geboortedatum: </label>
                    </div>
                    <div class="input-group mb-3">
                        <input type="date" name="birthday" class="form-control {{ $errors->has('birthday') ? 'is-invalid' : '' }}" value="{{ old('birthday') }}"
                               placeholder="yyyy/mm/dd">
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fas fa-user"></span>
                            </div>
                        </div>

                        @if ($errors->has('birthday'))
                            <div class="invalid-feedback">
                                <strong>{{ $errors->first('birthday') }}</strong>
                                </span>
                                @endif
                            </div>
                            <div>
                                <label>Ik wil graag per mail informatie ontvangen over: </label>
                            </div>
                            <p>
                                Geef in het opmerking veld hieronder op welk huisnummer je op dit moment woont.
                                Indien je een van de zorgvragende bewoner(s) ondersteund geef dan aan om welke huisnummer(s) dit gaat. <br>
                                Stuur in alle gevallen na het invullen van dit formulier een mail naar <a href="mailto:bestuur@demolen.me">bestuur@demolen.me</a>.
                                Het bestuur zal je inschrijving vervolgens verwerken.
                            </p>
                            <div style="margin-top:10px;">
                                <label>Opmerkingen: </label>
                            </div>
                            <div class="input-group mb-3">
                                <textarea rows="10" name="motivation" class="form-control {{ $errors->has('motivation') ? 'is-invalid' : '' }}" placeholder="">{{ old('motivation') }}</textarea>
                                <div class="input-group-append">
                                    <div class="input-group-text">
                                        <span class="fas fa-info"></span>
                                    </div>
                                </div>

                                @if ($errors->has('motivation'))
                                    <div class="invalid-feedback">
                                        <strong>{{ $errors->first('motivation') }}</strong>
                                    </div>
                                @endif
                            </div>
                            <div style="margin-top:10px;">
                                <label>Ik ga akkoord met de &nbsp;<a HREF="https://www.demolen.me/privacy-policy" target="_blank">privacyverklaring:</a> </label>
                            </div>
                            <div class="input-group">
                                <input type="checkbox" name="privacyVoorwaarde" class="form-control {{ $errors->has('privacyVoorwaarde') ? 'is-invalid' : '' }}">
                                @if ($errors->has('privacyVoorwaarde'))
                                    <div class="invalid-feedback">
                                        <strong>{{ $errors->first('privacyVoorwaarde') }}</strong>
                                    </div>
                                @endif
                            </div>
                            <button type="submit" class="btn btn-primary btn-block btn-flat">
                                Inschrijven
                            </button>
                    </div>
                </form>
                <p class="mt-2 mb-1">
                    <a href="{{ url(config('adminlte.login_url', 'login')) }}">
                        Ik sta al ingeschreven
                    </a>
                </p>
            </div>
            <!-- /.form-box -->
        </div><!-- /.register-box -->

@stop

@section('adminlte_js')
    <script src="{{ asset('vendor/adminlte/dist/js/adminlte.min.js') }}"></script>
    @stack('js')
    @yield('js')
    <script>
        var minLength = 1000;
        $('textarea').keyup(function() {
            var length = $(this).val().length;
            var length = minLength-length;
            $('#chars').text(length);
        });
    </script>
@stop
