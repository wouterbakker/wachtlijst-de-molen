@component('mail::message')
{{-- Greeting --}}
Hi,<br>
<br>

Bedankt voor je inschrijving! Vanaf nu zit je in onze database en zullen we je op de hoogte houden van vrijgekomen woningen van jouw voorkeur. Jouw inschrijfnummer is te vinden in jouw persoonlijk profiel op de website. <br>
<br>
Wil je je alvast inlezen in onze selectieprocedure? Dit kan via deze link: <a href="https://www.demolen.me/wonen-in-de-molen/">https://www.demolen.me/wonen-in-de-molen/</a>  <br>
<br>
Met vriendelijke groet, <br>
De selectiecommissie van wooncomplex de Molen

@endcomponent
