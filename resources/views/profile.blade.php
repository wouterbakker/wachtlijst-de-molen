@extends('adminlte::page')

@section('title', 'Mijn Account - De Molen')

@section('content_header')
    <div style="text-align:center;">
        <h1>Mijn account</h1>
    </div>
@stop

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                @if(Session::has('messageSucces'))
                    <div class="alert alert-success">
                        {{ Session::get('messageSucces') }}
                    </div>
                @endif
                <div>
                    <label>Inschrijfnummer: {{ Auth::user()->id }}</label>
                </div>
                <form action="{{ route('updateuser') }}" method="post">
                    {{ csrf_field() }}
                    <div>
                        <label>Naam: </label>
                    </div>
                    <div class="input-group mb-3">
                        <input type="text" name="name" disabled="disabled" class="form-control {{ $errors->has('name') ? 'is-invalid' : '' }}" value="{{ Auth::user()->name }}">
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fas fa-user"></span>
                            </div>
                        </div>
                    </div>
                    <p>
                        Het is niet mogelijk om je naam te wijzigen. Klopt je naam niet? Stuur dan een mail naar: <a href="mailto:bestuur@demolen.me">bestuur@demolen.me</a>
                    </p>
                    <div>
                        <label>E-mail: </label>
                    </div>
                    <div class="input-group mb-3">
                        <input type="email" name="email" class="form-control {{ $errors->has('email') ? 'is-invalid' : '' }}" value="{{ Auth::user()->email }}"
                               placeholder="voornaam@demolen.me">
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fas fa-envelope"></span>
                            </div>
                        </div>
                        @if ($errors->has('email'))
                            <div class="invalid-feedback">
                                <strong>{{ $errors->first('email') }}</strong>
                            </div>
                        @endif
                    </div>
                    <div>
                        <label>Geboortedatum: </label>
                    </div>
                    <div class="input-group mb-3">
                        <input type="date" name="birthday" class="form-control {{ $errors->has('birthday') ? 'is-invalid' : '' }}" value="{{ Auth::user()->birthday }}"
                               placeholder="dd/mm/yyyy">
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fas fa-user"></span>
                            </div>
                        </div>

                        @if ($errors->has('birthday'))
                            <div class="invalid-feedback">
                                <strong>{{ $errors->first('birthday') }}</strong>
                            </div>
                        @endif
                    </div>
                    <div>
                        <label>Ik wil graag per mail informatie ontvangen over: </label>
                    </div>
                    <div class="input-group">
                        <label class="form-check-label"> <input type="checkbox" name="studio" @if (Auth::user()->wantStudioMail) checked @endif>Een studio</label>
                    </div>
                    <div class="input-group">
                        <label class="form-check-label"><input type="checkbox" name="tweeKamer" @if (Auth::user()->wantTweeKamerMail) checked @endif>2-kamer appartement </label>
                    </div>
                    <div class="input-group">
                        <label class="form-check-label"><input type="checkbox" name="drieKamer" @if (Auth::user()->wantDrieKamerMail) checked @endif>3-kamer appartement </label>
                    </div>
                    <div class="input-group">
                        <label class="form-check-label"><input type="checkbox" name="vierKamer" @if (Auth::user()->wantVierKamerMail) checked @endif>4-kamer appartement </label>
                    </div>
                    <div class="input-group">
                        <label class="form-check-label"><input type="checkbox" name="jongerenWoning" @if (Auth::user()->wantJongerenWoningMail) checked @endif>Jongeren woning (maximaal 23 jaar) </label>
                    </div>
                    <div style="margin-top:10px;">
                        <label>Motivatie (minimaal nog <span id="chars">1000</span> tekens): </label>
                    </div>
                    <div class="input-group mb-3">
                        <textarea rows="10" name="motivation" class="form-control {{ $errors->has('motivation') ? 'is-invalid' : '' }}" placeholder="">{{ Auth::user()->motivation }}</textarea>
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fas fa-info"></span>
                            </div>
                        </div>

                        @if ($errors->has('motivation'))
                            <div class="invalid-feedback">
                                <strong>{{ $errors->first('motivation') }}</strong>
                            </div>
                        @endif
                    </div>
                    <button type="submit" class="btn btn-primary btn-block btn-flat">
                        Bijwerken
                    </button>
                </form>
                <form action="{{ route('updatePassword') }}" method="post">
                    <div>
                        {{ csrf_field() }}
                        @if(Session::has('messagePasswordSucces'))
                            <div class="alert alert-success">
                                {{ Session::get('messagePasswordSucces') }}
                            </div>
                        @endif
                        @if(Session::has('messagePasswordError'))
                            <div class="alert alert-danger">
                                {{ Session::get('messagePasswordError') }}
                            </div>
                        @endif
                        <div>
                            <label>Huidige wachtwoord: </label>
                        </div>
                        <div class="input-group mb-3">
                            <input type="password" name="oldPassword" class="form-control {{ $errors->has('oldPassword') ? 'is-invalid' : '' }}"
                                   placeholder="Wachtwoord">
                            <div class="input-group-append">
                                <div class="input-group-text">
                                    <span class="fas fa-lock"></span>
                                </div>
                            </div>
                            @if ($errors->has('oldPassword'))
                                <div class="invalid-feedback">
                                    <strong>{{ $errors->first('oldPassword') }}</strong>
                                </div>
                                </span>
                            @endif
                        </div>
                        <div>
                            <label>Nieuw wachtwoord: </label>
                        </div>
                        <div class="input-group mb-3">
                            <input type="password" name="password" class="form-control {{ $errors->has('password') ? 'is-invalid' : '' }}"
                                   placeholder="Wachtwoord">
                            <div class="input-group-append">
                                <div class="input-group-text">
                                    <span class="fas fa-lock"></span>
                                </div>
                            </div>
                            @if ($errors->has('password'))
                                <div class="invalid-feedback">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </div>
                            @endif
                        </div>
                        <div class="input-group mb-3">
                            <input type="password" name="password_confirmation" class="form-control {{ $errors->has('password_confirmation') ? 'is-invalid' : '' }}"
                                   placeholder="Herhaal wachtwoord">
                            <div class="input-group-append">
                                <div class="input-group-text">
                                    <span class="fas fa-lock"></span>
                                </div>
                            </div>
                            @if ($errors->has('password_confirmation'))
                                <div class="invalid-feedback">
                                    <strong>{{ $errors->first('password_confirmation') }}</strong>
                                </div>
                            @endif
                        </div>
                        <button type="submit" class="btn btn-primary btn-block btn-flat">
                            Bijwerken
                        </button>
                    </div>
                </form>
                <form class="delete" action="{{ route('deleteUser') }}" method="post">
                    <div>
                        {{ csrf_field() }}
                        <div>
                            <label>Uitschrijven </label>
                        </div>
                        <p>
                            Door op onderstaande knop te klikken worden al je gegevens verwijderd. Indien je je daarna opnieuw inschrijft sta je weer onderaan de wachtlijst.
                        </p>
                        <button type="submit" class="btn btn-primary btn-danger btn-block btn-flat">
                            Verwijder al mijn gegevens en haal me van de wachtlijst
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@stop

@section('css')

@stop

@section('js')
    @stack('js')
    @yield('js')
    <script>
        var minLength = 1000;
        $('textarea').keyup(function() {
            var length = $(this).val().length;
            var length = minLength-length;
            $('#chars').text(length);
        });
    </script>

    <script>
        $(".delete").on("submit", function(){
            return confirm("Weet je het heel zeker? Deze knop werkt echt, al je gegevens zullen worden verwijderd.");
        });
    </script>


@endsection
