@extends('adminlte::page')

@section('title', 'Wachtlijst nummer')

@section('content_header')
    <div style="text-align:center;">
        <h1>Wachtlijstnummer</h1>
    </div>

@stop

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-body">
                        <div style="text-align:center;">
                            <h2> Je staat op dit moment op plek <b>{{ $waitingListNumber ?? 'onbekend' }}</b> van de wachtlijst. </h2>
                            <br>
                            <h2>Je inschrijfnummer is <b>{{ Auth::user()->id }}</b>. Dit is een uniek nummer van jouw inschrijving en zal niet veranderen.<br>
                                Geef altijd het inschrijfnummer door als je reageert op een woning. </h2><br>
                            <h2>Wil je iets veranderen aan je inschrijving, klik dan in het menu op profiel.</h2><br>
                            <h2>Ben een bewoner of ondersteun je een zorgvragende bewoner en zie je dit scherm. Je inschrijving is in dat geval nog niet volledig verwerkt. Stuur daarom een mail naar <a href="mailto:bestuur@demolen.me">bestuur@demolen.me</a>.
                                Het bestuur zal je inschrijving vervolgens verwerken. </h2>
                        </div>
                    </div>
                    <!-- /.box -->
                </div>
            </div>
        </div>
    </div>
@stop

@section('css')

@stop
