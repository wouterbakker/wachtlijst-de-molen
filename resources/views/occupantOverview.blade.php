@extends('adminlte::page')

@section('title', 'Bewonersoverzicht')

@section('content_header')
    <div style="text-align:center;">
        <h1>Bewonersoverzicht de Molen</h1>
    </div>
@stop

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header">
                        @if(Session::has('message'))
                            <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
                        @endif
                    </div>
                    <div style="text-align:center;">
                        <p>Klik op het inschrijfnummer om de gegevens te bewerken</p>
                    </div>
                    <div class="box-body">
                        <table id="occupantoverview" class="table table-bordered table-hover dataTable">
                            <thead>
                            <tr>
                                <th>Inschrijfnummer</th>
                                <th>Huisnummer</th>
                                <th>Naam</th>
                                <th>Email</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                </div>
                <!-- /.box -->
            </div>
            <div class="col-md-3" style="margin-bottom: 10px; margin-top: 10px;">
                <a class="btn btn-primary btn-block btn-flat" href="{{action('occupantOverviewController@exportAllOccupants')}}">Export alle bewoners</a>
            </div>
        </div>
    </div>
@stop

@section('css')
    <link href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css" rel="stylesheet">
    <link href="https://cdn.datatables.net/buttons/1.6.1/css/buttons.jqueryui.min.css" rel="stylesheet">

@stop
@section('js')
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.1/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
    <script src="/vendor/datatables/buttons.server-side.js"></script>

    <script type="text/javascript" language="javascript" >
        $(document).ready(function(){
            $('#occupantoverview').DataTable({
                processing: true,
                serverSide: true,
                ajax: '{{ route("occupantoverview.index") }}',
                "order": [[1, 'asc']],
                columns: [
                    { data: 'id', name: 'id', render: function(data, type, full, meta) {return '<a href="admin/user/'+data+'">'+ data + '</a>'}},
                    { data: 'houseNumber', name:'houseNumber'},
                    { data: 'name', name: 'name' },
                    { data: 'email', name: 'email' },
                ],
            });
        });
    </script>
@stop
