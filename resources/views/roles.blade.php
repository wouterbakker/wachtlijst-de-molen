@extends('adminlte::page')

@section('title', 'Rollenoverzicht')

@section('content_header')

  <div style="text-align:center;">
    <h1>Rollenoverzicht</h1>
  </div>

@stop

@section('content')
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="box">
          <div class="box-body">
            <div style="text-align:center;">
              <h3> Super Admins </h3>
              @foreach ($usersSuperAdmins as $superAdmin)
                {{ $superAdmin->name }} - {{ $superAdmin->email}} <a href="{{config('app.url')}}/admin/user/{{$superAdmin->id}}">edit</a><br>
              @endforeach
              <br>
              <h3> Wachtlijst-editor</h3>
              @foreach ($usersWaitingListAdmins as $waitingListAdmin)
                {{ $waitingListAdmin->name }} - {{ $waitingListAdmin->email}} <a href="{{config('app.url')}}/admin/user/{{$waitingListAdmin->id}}">edit</a><br>
              @endforeach
              <br>
              <h3> Wachtlijst bekijken</h3>
              @foreach ($usersWaitingListViewers as $waitingListViewer)
                {{ $waitingListViewer->name }} - {{ $waitingListViewer->email}} <a href="{{config('app.url')}}/admin/user/{{$waitingListViewer->id}}">edit</a><br>
              @endforeach

            </div>
          </div>
          <!-- /.box -->
        </div>
      </div>
    </div>
  </div>
@stop

@section('css')

@stop
