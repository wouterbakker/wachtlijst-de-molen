@extends('adminlte::master')

@section('adminlte_css')
    @yield('css')
    <link href="{{ asset('css/register.css') }}" rel="stylesheet" type="text/css" >
@stop

@section('classes_body', 'register-page')

@section('body')
    <div class="register-box">
        <div class="register-logo">
            <a href="{{ url(config('adminlte.dashboard_url', 'home')) }}">{!! config('adminlte.logo', '<b>Wachtlijst</b>de Molen') !!}</a>
        </div>
        <div class="card">
            <div class="card-body register-card-body">
                <p class="login-box-msg"> <b> Schrijf je nu voor onze wachtlijst. </b></p>
            <form action="{{ url(config('adminlte.register_url', 'register')) }}" method="post">
                {{ csrf_field() }}
                <div>
                  <label>Naam: </label>
                </div>
                <div class="input-group mb-3">
                    <input type="text" name="name" class="form-control {{ $errors->has('name') ? 'is-invalid' : '' }}" value="{{ old('name') }}"
                           placeholder="Voornaam Achternaam">
                    <div class="input-group-append">
                        <div class="input-group-text">
                            <span class="fas fa-user"></span>
                        </div>
                    </div>

                    @if ($errors->has('name'))
                        <div class="invalid-feedback">
                            <strong>{{ $errors->first('name') }}</strong>
                        </div>
                    @endif
                </div>
                <div>
                  <label>E-mail: </label>
                </div>
                <div class="input-group mb-3">
                    <input type="email" name="email" class="form-control {{ $errors->has('email') ? 'is-invalid' : '' }}" value="{{ old('email') }}"
                           placeholder="voornaam@demolen.me">
                    <div class="input-group-append">
                        <div class="input-group-text">
                            <span class="fas fa-envelope"></span>
                        </div>
                    </div>
                    @if ($errors->has('email'))
                        <div class="invalid-feedback">
                            <strong>{{ $errors->first('email') }}</strong>
                        </div>
                    @endif
                </div>
                <div>
                  <label>Wachtwoord: </label>
                </div>
                <div class="input-group mb-3">
                    <input type="password" name="password" class="form-control {{ $errors->has('password') ? 'is-invalid' : '' }}"
                           placeholder="Wachtwoord">
                    <div class="input-group-append">
                        <div class="input-group-text">
                            <span class="fas fa-lock"></span>
                        </div>
                    </div>
                    @if ($errors->has('password'))
                        <div class="invalid-feedback">
                            <strong>{{ $errors->first('password') }}</strong>
                        </div>
                    @endif
                </div>
                <div class="input-group mb-3">
                    <input type="password" name="password_confirmation" class="form-control {{ $errors->has('password_confirmation') ? 'is-invalid' : '' }}"
                           placeholder="Herhaal wachtwoord">
                    <div class="input-group-append">
                        <div class="input-group-text">
                            <span class="fas fa-lock"></span>
                        </div>
                    </div>
                    @if ($errors->has('password_confirmation'))
                        <div class="invalid-feedback">
                            <strong>{{ $errors->first('password_confirmation') }}</strong>
                        </div>
                    @endif
                </div>
                <div>
                  <label>Geboortedatum: </label>
                </div>
                <div class="input-group mb-3">
                    <input type="date" name="birthday" class="form-control {{ $errors->has('birthday') ? 'is-invalid' : '' }}" value="{{ old('birthday') }}"
                           placeholder="yyyy/mm/dd">
                    <div class="input-group-append">
                        <div class="input-group-text">
                            <span class="fas fa-user"></span>
                        </div>
                    </div>

                    @if ($errors->has('birthday'))
                        <div class="invalid-feedback">
                            <strong>{{ $errors->first('birthday') }}</strong>
                        </span>
                    @endif
                </div>
                <div>
                  <label>Ik wil graag per mail informatie ontvangen over: </label>
                </div>
                <div class="input-group">
                    <label class="form-check-label"> <input type="checkbox" name="studio" @if (old('studio')==="on") checked @endif>Een studio</label>
                </div>
                <div class="input-group">
                <label class="form-check-label"><input type="checkbox" name="tweeKamer" @if (old('tweeKamer')==="on") checked @endif>2-kamer appartement </label>
                </div>
                <div class="input-group">
                    <label class="form-check-label"><input type="checkbox" name="drieKamer" @if (old('drieKamer')==="on") checked @endif>3-kamer appartement </label>
                </div>
                <div class="input-group">
                    <label class="form-check-label"><input type="checkbox" name="vierKamer" @if (old('vierKamer')==="on") checked @endif>4-kamer appartement </label>
                </div>
                <div class="input-group">
                    <label class="form-check-label"><input type="checkbox" name="jongerenWoning" @if (old('jongerenWoning')==="on") checked @endif>Jongeren woning (maximaal 23 jaar) </label>
                </div>
                <div style="margin-top:10px;">
                  <label>Motivatie (minimaal nog <span id="chars">1000</span> tekens): </label>
                </div>
                <div class="input-group mb-3">
                    <textarea rows="10" name="motivation" class="form-control {{ $errors->has('motivation') ? 'is-invalid' : '' }}" placeholder="">{{ old('motivation') }}</textarea>
                    <div class="input-group-append">
                      <div class="input-group-text">
                          <span class="fas fa-info"></span>
                      </div>
                    </div>

                    @if ($errors->has('motivation'))
                        <div class="invalid-feedback">
                            <strong>{{ $errors->first('motivation') }}</strong>
                        </div>
                    @endif
                </div>
                <div style="margin-top:10px;">
                  <label>Ik ga akkoord met de &nbsp;<a HREF="https://www.demolen.me/privacy-policy" target="_blank">privacyverklaring:</a> </label>
                </div>
                <div class="input-group">
                    <input type="checkbox" name="privacyVoorwaarde" class="form-control {{ $errors->has('privacyVoorwaarde') ? 'is-invalid' : '' }}">
                    @if ($errors->has('privacyVoorwaarde'))
                        <div class="invalid-feedback">
                            <strong>{{ $errors->first('privacyVoorwaarde') }}</strong>
                        </div>
                    @endif
                </div>
                <button type="submit" class="btn btn-primary btn-block btn-flat">
                    Inschrijven
                </button>
            </form>
            <p class="mt-2 mb-1">
                <a href="{{ url(config('adminlte.login_url', 'login')) }}">
                    Ik sta al ingeschreven
                </a>
            </p>
        </div>
        <!-- /.form-box -->
    </div><!-- /.register-box -->
@stop

@section('adminlte_js')
    <script src="{{ asset('vendor/adminlte/dist/js/adminlte.min.js') }}"></script>
    @stack('js')
    @yield('js')
    <script>
    var minLength = 1000;
      $('textarea').keyup(function() {
        var length = $(this).val().length;
        var length = minLength-length;
        $('#chars').text(length);
      });
    </script>
@stop
