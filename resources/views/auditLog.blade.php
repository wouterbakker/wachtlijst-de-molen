@extends('adminlte::page')

@section('title', 'Auditlog')

@section('content_header')
  <div style="text-align:center;">
    <h1>Audit log</h1>
  </div>

@stop

@section('content')
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="box">
          <div class="box-header">
            @if(Session::has('message'))
              <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
            @endif
          </div>
          <div style="text-align:center;">
            <p>Klik op het inschrijfnummer om de gegevens te bewerken</p>
          </div>
          <div class="box-body">
            <table id="waitinglist" class="table table-bordered table-hover dataTable">
              <thead>
                <tr>
                  <th>Id</th>
                  <th>Tijd (UTC)</th>
                  <th>Log</th>
                </tr>
              </thead>
            </table>
          </div>
        </div>
        <!-- /.box -->
      </div>
    </div>
  </div>

@stop

@section('css')
  <link href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css" rel="stylesheet">
  <link href="https://cdn.datatables.net/buttons/1.6.1/css/buttons.jqueryui.min.css" rel="stylesheet">
  <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.standalone.min.css" rel="stylesheet">

@stop
@section('js')
  <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
  <script src="https://cdn.datatables.net/buttons/1.6.1/js/dataTables.buttons.min.js"></script>
  <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
  <script src="/vendor/datatables/buttons.server-side.js"></script>

  <script type="text/javascript" language="javascript" >
  $(document).ready(function(){
    $('#waitinglist').DataTable({
      processing: true,
      serverSide: true,
      ajax: '{{ route("auditlog.index") }}',
      columns: [
        { data: 'id', name: 'id' },
        { data: 'created_at', name: 'created_at' },
        { data: 'log', name: 'log' },
      ],
    });
  });
</script>
@stop
