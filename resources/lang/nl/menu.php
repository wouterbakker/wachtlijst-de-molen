<?php

return [

    'main_navigation'               => 'MAIN MENU',
    'blog'                          => 'Blog',
    'pages'                         => 'Pagina\'s',
    'account_settings'              => 'ACCOUNT INSTELLINGEN',
    'profile'                       => 'Profiel',
    'change_password'               => 'Verander Wachtwoord',
    'multilevel'                    => 'Multi Level',
    'level_one'                     => 'Level 1',
    'level_two'                     => 'Level 2',
    'level_three'                   => 'Level 3',
    'labels'                        => 'LABELS',
    'important'                     => 'Belangrijk',
    'warning'                       => 'Waarschuwing',
    'information'                   => 'Informatie',
    'waiting_list'                  => 'Wachtlijst',
    'room_list'                     => 'Appartementen',
];
