<?php

return [

    'full_name'                   => 'Volledige naam',
    'email'                       => 'E-mailadres',
    'password'                    => 'Wachtwoord',
    'retype_password'             => 'Wachtwoord nogmaals invoeren',
    'remember_me'                 => 'Ingelogd blijven',
    'register'                    => 'Registreren',
    'register_a_new_membership'   => 'Schrijf je in voor de wachtlijst',
    'i_forgot_my_password'        => 'Ik ben mijn wachtwoord vergeten',
    'i_already_have_a_membership' => 'Ik sta al op de wachtlijst',
    'sign_in'                     => 'Inloggen',
    'log_out'                     => 'Uitloggen',
    'toggle_navigation'           => 'Schakel navigatie',
    'login_message'               => 'Log in',
    'register_message'            => 'Schrijf je in voor de wachtlijst',
    'password_reset_message'      => 'Wachtwoord herstellen',
    'reset_password'              => 'Wachtwoord herstellen',
    'send_password_reset_link'    => 'Verzend link voor wachtwoordherstel',
];
