<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//basic routes
Route::get('/', function () {return redirect('waitinglistnumber');});
Auth::routes();

//User routes
Route::get('waitinglistnumber', 'waitingListController@waitingListNumber')->middleware('auth');
Route::get('profile', function () {return view('profile');})->middleware('auth');
Route::get('registerOccupant', function () {return view('registerOccupant');})->name('registerOccupant');
Route::post('updateuser','updateUserController@update')->name('updateuser')->middleware('auth');
Route::post('updatePassword','updateUserController@updatePassword')->name('updatePassword')->middleware('auth');
Route::post('deleteUser','updateUserController@deleteUser')->name('deleteUser')->middleware('auth');

//Admin routes
Route::resource('waitinglist', 'waitingListController')->middleware('can:viewWaitingList');
Route::resource('occupantoverview', 'occupantOverviewController')->middleware('can:viewWaitingList');
Route::get('admin/user/{id}', 'showProfileController')->name('AdminShowUser')->middleware('can:viewWaitingList');
Route::post('admin/edituser/{id}', 'AdminEditUserController@update')->name('AdminEditUser')->middleware('can:editUser');
Route::post('admin/resetwaitinglist/{id}', 'AdminEditUserController@resetWaitingList')->name('AdminResetWaitingList')->middleware('can:editWaitingList');
Route::post('admin/deleteuser/{id}', 'AdminEditUserController@deleteUser')->name('AdminDeleteUser')->middleware('can:editUser');
Route::get('admin/roles', 'showRolesController')->name('AdminShowRoles')->middleware('can:editPermissions');
Route::resource('admin/auditlog', 'auditLogController')->middleware('can:editPermissions');

//Export Routes
Route::get('exportall', 'waitingListController@exportAll')->middleware('can:viewWaitingList');
Route::get('exportstudio', 'waitingListController@exportStudio')->middleware('can:viewWaitingList');
Route::get('exporttweekamer', 'waitingListController@exportTweeKamer')->middleware('can:viewWaitingList');
Route::get('exportdriekamer', 'waitingListController@exportDrieKamer')->middleware('can:viewWaitingList');
Route::get('exportvierkamer', 'waitingListController@exportVierKamer')->middleware('can:viewWaitingList');
Route::get('exportjongeren', 'waitingListController@exportJongeren')->middleware('can:viewWaitingList');
Route::get('exportalloccupants', 'occupantOverviewController@exportAllOccupants')->middleware('can:viewWaitingList');
