<?php

namespace App\Exports;

use App\User;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class allOccupantExport implements FromCollection, WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return User::all()->where('occupant','=',true);
    }
    public function headings(): array
    {
    return [
        'Inschrijfnummer',
        'Naam',
        'E-mail',
        'email_verified_at',
        'Created at',
        'Updated at',
        'Geboortedatum',
        'Motivatie',
        'Datum voor wachtlijst',
        'Studio',
        '2 Kamer',
        '3 Kamer',
        '4 Kamer',
        'Jongeren woning',
        'Is bewoner',
        'Huisnummer',
    ];
  }
}
