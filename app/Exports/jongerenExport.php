<?php

namespace App\Exports;

use App\User;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class jongerenExport implements FromCollection, WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return User::select(['name','email'])->where('wantJongerenWoningMail','1')->get();
    }
    public function headings(): array
    {
    return [
        'Naam',
        'E-mail',
    ];
  }
}
