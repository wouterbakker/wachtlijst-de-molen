<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class auditLog extends Model
{
  protected $fillable = [
      'log',
  ];
}
