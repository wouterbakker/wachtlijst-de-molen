<?php

namespace App\Http\Controllers;

use App\Exports\allOccupantExport;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use App\User;
use Maatwebsite\Excel\Facades\Excel;


class occupantOverviewController extends Controller
{
  public function index(Request $request)
  {
    if(request()->ajax()) {
      $users = User::select('id','name', 'email','houseNumber','occupant')->where('occupant','=',true);
      return (new DataTables)->eloquent($users)->toJson();
    }
    else{
      return view('occupantOverview');
    }

  }
    public function exportAllOccupants()
    {
        return Excel::download(new allOccupantExport, 'bewoners.xlsx');
    }
}
