<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class showProfileController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke($id)
    {
        $user = User::findOrFail($id);
        $waitingListNumber = User::where('waitingListDate','<',$user->waitingListDate)->where('occupant','=',false)->count() +1;
        return view('edituser')
          ->with('user',$user)
          ->with('waitingListNumber',$waitingListNumber);
    }
}
