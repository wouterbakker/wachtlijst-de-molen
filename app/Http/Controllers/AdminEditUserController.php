<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Hash;
use App\User;
use App\auditLog;
use DateTime;

class AdminEditUserController extends Controller
{
  public function update(Request $data, $id){

      $adminUser = Auth::user();
      $user = User::findOrFail($id);

      if ($adminUser->cannot('editUser')){
        $data->session()->flash('messageError', 'Geen rechten');
        return redirect()->route('AdminShowUser', ['id' => $user->id]);
      }

      $data->validate([
          'name' => ['required', 'string', 'max:255'],
          'email' => ['required', 'string', 'email', 'max:255', Rule::unique('users')->ignore($user->id, 'id')],
          'birthday' => ['required','date'],
          'motivation' => ['required','string'],
      ]);

      if (isset($data['studio'])){
        //user wants mail over a studio
        $wantStudioMail = true;
      }else{
        $wantStudioMail = false;
      }
      if (isset($data['tweeKamer'])){
        //user wants mail over a 2-kamer appartement
        $wantTweeKamerMail = true;
      }else{
        $wantTweeKamerMail = false;
      }
      if (isset($data['drieKamer'])){
        //user wants mail over a 3-kamer appartement
        $wantDrieKamerMail = true;
      }else{
        $wantDrieKamerMail = false;
      }
      if (isset($data['vierKamer'])){
        //user wants mail over a 4-kamer appartement
        $wantVierKamerMail = true;
      }else{
        $wantVierKamerMail = false;
      }
      if (isset($data['jongerenWoning'])){
        //user wants mail over a 4-kamer appartement
        $wantJongerenWoningMail = true;
      }else{
        $wantJongerenWoningMail = false;
      }
      if (isset($data['occupant'])){
          //user is occupant
          $occupant = true;
      }else{
          $occupant = false;
      }
     $user->update([
          'name' => $data['name'],
          'email' => $data['email'],
          'birthday'=> $data['birthday'],
          'motivation' => $data['motivation'],
          'wantStudioMail' => $wantStudioMail,
          'wantTweeKamerMail' => $wantTweeKamerMail,
          'wantDrieKamerMail' => $wantDrieKamerMail,
          'wantVierKamerMail' => $wantVierKamerMail,
          'wantJongerenWoningMail' => $wantJongerenWoningMail,
          'occupant' => $occupant,
          'houseNumber' => $data['houseNumber'],
      ]);

      if ($adminUser->can('editPermissions')){
        if (isset($data['waitingListViewer'])){
          if (!$user->hasRole('waitingListViewer')){
            $user->assignRole('waitingListViewer');
            $logentry = 'Admin with id ' . $adminUser->id . ' and name ' . $adminUser->name . ' gives the admin role waitingListViewer to user with id  ' . $user->id .' (' . $user->name . ')' ;
            auditLog::create(['log' => $logentry]);
          }
        }else{
          if ($user->hasRole('waitingListViewer')){
            $user->removeRole('waitingListViewer');
            $logentry = 'Admin with id ' . $adminUser->id . ' and name ' . $adminUser->name . ' removes the admin role waitingListViewer from user with id ' . $user->id .' (' . $user->name . ').' ;
            auditLog::create(['log' => $logentry]);
          }
        }
        if (isset($data['waitingListAdmin'])){
          if (!$user->hasRole('waitingListAdmin')){
            $user->assignRole('waitingListAdmin');
            $logentry = 'Admin with id ' . $adminUser->id . ' and name ' . $adminUser->name . ' gives the admin role waitingListAdmin to user with id ' . $user->id .' (' . $user->name . ').' ;
            auditLog::create(['log' => $logentry]);
          }
        }else{
          if ($user->hasRole('waitingListAdmin')){
            $user->removeRole('waitingListAdmin');
            $logentry = 'Admin with id ' . $adminUser->id . ' and name ' . $adminUser->name . ' removes the admin role waitingListAdmin from user with id ' . $user->id .' (' . $user->name . ').' ;
            auditLog::create(['log' => $logentry]);
          }
        }
        if (isset($data['superAdmin'])){
          if (!$user->hasRole('superAdmin')){
            $user->assignRole('superAdmin');
            $logentry = 'Admin with id ' . $adminUser->id . ' and name ' . $adminUser->name . ' gives the admin role superAdmin to user with id ' . $user->id .' (' . $user->name . ').' ;
            auditLog::create(['log' => $logentry]);
          }
        }else{
          if ($user->hasRole('superAdmin')){
            $user->removeRole('superAdmin');
            $logentry = 'Admin with id ' . $adminUser->id . ' and name ' . $adminUser->name . ' removes the admin role superAdmin from user with id ' . $user->id .' (' . $user->name . ').' ;
            auditLog::create(['log' => $logentry]);
          }
        }
      }
      $logentry = 'Admin with id ' . $adminUser->id . ' and name ' . $adminUser->name . ' edits user with id ' . $user->id .' (' . $user->name . ').' ;
      auditLog::create(['log' => $logentry]);
      $user->save();
      $data->session()->flash('messageSucces', 'Bijwerken gelukt');
      return redirect()->route('AdminShowUser', ['id' => $user->id]);
  }
  public function resetWaitinglist(Request $data, $id){
    $adminUser = Auth::user();
    $user = User::findOrFail($id);

    $logentry = 'Admin with id ' . $adminUser->id . ' and name ' . $adminUser->name . ' reset the waiting list for user with id ' . $user->id .' (' . $user->name . '). Old value ' .  $user->waitingListDate;
    auditLog::create(['log' => $logentry]);
    $user->update([
      'waitingListDate' => new DateTime(),
     ]);


    $user->save();
    $data->session()->flash('messageSucces', 'Gebruiker is onderaan de wachtlijst geplaatst');
      return redirect()->route('AdminShowUser', ['id' => $user->id]);
  }
  public function deleteUser($id){
    $adminUser = Auth::user();
    $user = User::findOrFail($id);

    $logentry = 'Admin with id ' . $adminUser->id . ' and name ' . $adminUser->name . ' delete user ' . $user->id .' (' . $user->name . '). Waiting list date was: ' .  $user->waitingListDate;
    auditLog::create(['log' => $logentry]);

    $user->delete();

    return redirect('waitinglist');
  }
}
