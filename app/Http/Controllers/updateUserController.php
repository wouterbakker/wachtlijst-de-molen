<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Hash;
use App\User;

class updateUserController extends Controller
{
  public function update(Request $data){

      $user = Auth::user();

      $data->validate([
          'email' => ['required', 'string', 'email', 'max:255', Rule::unique('users')->ignore($user->id, 'id')],
          'birthday' => ['required','date'],
      ]);

      if (isset($data['studio'])){
        //user wants mail over a studio
        $wantStudioMail = true;
      }else{
        $wantStudioMail = false;
      }
      if (isset($data['tweeKamer'])){
        //user wants mail over a 2-kamer appartement
        $wantTweeKamerMail = true;
      }else{
        $wantTweeKamerMail = false;
      }
      if (isset($data['drieKamer'])){
        //user wants mail over a 3-kamer appartement
        $wantDrieKamerMail = true;
      }else{
        $wantDrieKamerMail = false;
      }
      if (isset($data['vierKamer'])){
        //user wants mail over a 4-kamer appartement
        $wantVierKamerMail = true;
      }else{
        $wantVierKamerMail = false;
      }
      if (isset($data['jongerenWoning'])){
        //user wants mail over a 4-kamer appartement
        $wantJongerenWoningMail = true;
      }else{
        $wantJongerenWoningMail = false;
      }
     $user->update([
          'email' => $data['email'],
          'birthday'=> $data['birthday'],
          'motivation' => $data['motivation'],
          'wantStudioMail' => $wantStudioMail,
          'wantTweeKamerMail' => $wantTweeKamerMail,
          'wantDrieKamerMail' => $wantDrieKamerMail,
          'wantVierKamerMail' => $wantVierKamerMail,
          'wantJongerenWoningMail' => $wantJongerenWoningMail,
      ]);
      $user->save();
      $data->session()->flash('messageSucces', 'Bijwerken gelukt');
      return redirect('profile');
  }
  public function updatePassword(Request $data){
    $user = Auth::user();

    $data->validate([
      'password' => ['required', 'string', 'min:10','pwned','confirmed'],
    ]);

    if (Hash::check($data->oldPassword, $user->password)) {
      $user->update([
        'password' => Hash::make($data['password']),
      ]);
      $user->save();

      $data->session()->flash('messagePasswordSucces', 'Wachtwoord gewijzigd');
      return redirect('profile');

    } else {
        $data->session()->flash('messagePasswordError', 'Het oude wachtwoord is onjuist ');
        return redirect('profile');
    }
  }
  public function deleteUser(){
    $user = Auth::user();
    $user->delete();
    Auth::logout();
    return redirect('/');
  }
}
