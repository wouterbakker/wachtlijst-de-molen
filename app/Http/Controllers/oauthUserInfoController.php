<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Auth;

class oauthUserInfoController extends Controller
{
    public function get(Request $request)
    {
        $user = Auth::user();
        //check if user is an occupant
        if ($user->occupant){
            return $user;
        }
        else{
            return response('Unauthorized.', 401);
        }
    }
}
