<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class showRolesController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
      $usersSuperAdmins = User::role('superAdmin')->get();
      $usersWaitingListAdmins = User::role('waitingListAdmin')->get();
      $usersWaitingListViewers = User::role('waitingListViewer')->get();
      return view('roles')
        ->with('usersSuperAdmins',$usersSuperAdmins)
        ->with('usersWaitingListAdmins',$usersWaitingListAdmins)
        ->with('usersWaitingListViewers',$usersWaitingListViewers);
    }
}
