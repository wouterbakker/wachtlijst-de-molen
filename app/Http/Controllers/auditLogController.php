<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use App\auditLog;

class auditLogController extends Controller
{
  public function index(Request $request)
  {
    if(request()->ajax()) {
      $auditLog = auditLog::select('id','created_at', 'log')->orderBy('id','desc');
      return (new DataTables)->eloquent($auditLog)->toJson();
    }
    else{
      return view('auditLog');
    }

  }
}
