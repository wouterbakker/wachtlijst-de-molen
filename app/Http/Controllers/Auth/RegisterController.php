<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Mail\registrationSuccessful;
use App\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Mail;
use DateTime;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:10', 'pwned', 'confirmed'],
            'birthday' => ['required','date'],
            'motivation' => ['required','string'],
            'privacyVoorwaarde' => ['accepted'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
      if (isset($data['studio'])){
        //user wants mail over a studio
        $wantStudioMail = true;
      }else{
        $wantStudioMail = false;
      }
      if (isset($data['tweeKamer'])){
        //user wants mail over a 2-kamer appartement
        $wantTweeKamerMail = true;
      }else{
        $wantTweeKamerMail = false;
      }
      if (isset($data['drieKamer'])){
        //user wants mail over a 3-kamer appartement
        $wantDrieKamerMail = true;
      }else{
        $wantDrieKamerMail = false;
      }
      if (isset($data['vierKamer'])){
        //user wants mail over a 4-kamer appartement
        $wantVierKamerMail = true;
      }else{
        $wantVierKamerMail = false;
      }
      if (isset($data['jongerenWoning'])){
        //user wants mail over a 4-kamer appartement
        $wantJongerenWoningMail = true;
      }else{
        $wantJongerenWoningMail = false;
      }
        $user = User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
            'birthday'=> $data['birthday'],
            'motivation' => $data['motivation'],
            'waitingListDate' => new DateTime(),
            'wantStudioMail' => $wantStudioMail,
            'wantTweeKamerMail' => $wantTweeKamerMail,
            'wantDrieKamerMail' => $wantDrieKamerMail,
            'wantVierKamerMail' => $wantVierKamerMail,
            'wantJongerenWoningMail' => $wantJongerenWoningMail,
            'occupant' => false,
        ]);
        Mail::to($data['email'])->send(new registrationSuccessful($user));

        return $user;
    }
}
