<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\Exports\allUserExport;
use App\Exports\studioExport;
use App\Exports\tweeKamerExport;
use App\Exports\drieKamerExport;
use App\Exports\vierKamerExport;
use App\Exports\jongerenExport;
use Maatwebsite\Excel\Facades\Excel;


class waitingListController extends Controller
{
  public function index(Request $request)
  {
    if(request()->ajax()) {
      $users = User::select('id','name', 'email','waitingListDate')->where('occupant','=',false);
      return (new DataTables)->eloquent($users)->addColumn('waitingListNumber', function(User $user) {
                    $waitingListNumber = User::where('waitingListDate','<',$user->waitingListDate)->where('occupant','=',false)->count() +1;
                    return $waitingListNumber;
                })->toJson();
    }
    else{
      return view('waitingList');
    }

  }

  public function waitingListNumber(){
    $user = Auth::user();
    if ($user->occupant){
        return view('waitingListNumberOccupant');
    }
    else{
        $waitingListNumber = User::where('waitingListDate','<',$user->waitingListDate)->where('occupant','=',false)->count() +1;
        return view('waitingListNumber')->with('waitingListNumber',$waitingListNumber);
    }

  }

  public function exportAll()
  {
    return Excel::download(new allUserExport, 'wachtlijst.xlsx');
  }

  public function exportStudio()
  {
    return Excel::download(new studioExport, 'studio.xlsx');
  }

  public function exportTweeKamer()
  {
    return Excel::download(new tweeKamerExport, 'tweeKamer.xlsx');
  }

  public function exportDrieKamer()
  {
    return Excel::download(new drieKamerExport, 'drieKamer.xlsx');
  }

  public function exportVierKamer()
  {
    return Excel::download(new vierKamerExport, 'vierKamer.xlsx');
  }

  public function exportJongeren()
  {
    return Excel::download(new jongerenExport, 'jongerenWoning.xlsx');
  }

}
