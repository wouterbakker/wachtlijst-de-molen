<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddEmailPreferencesToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->boolean('wantStudioMail');
            $table->boolean('wantEenKamerMail');
            $table->boolean('wantTweeKamerMail');
            $table->boolean('wantDrieKamerMail');
            $table->boolean('wantVierKamerMail');
            $table->boolean('wantJongerenWoningMail');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
          $table->dropColumn('wantStudioMail');
          $table->dropColumn('wantEenKamerMail');
          $table->dropColumn('wantTweeKamerMail');
          $table->dropColumn('wantDrieKamerMail');
          $table->dropColumn('wantVierKamerMail');
          $table->dropColumn('wantJongerenWoningMail');  
        });
    }
}
