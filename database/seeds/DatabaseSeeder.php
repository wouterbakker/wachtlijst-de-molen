<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use Spatie\Permission\PermissionRegistrar;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        //Create Permissions
        Permission::create(['name' => 'viewWaitingList']);
        Permission::create(['name' => 'editWaitingList']);
        Permission::create(['name' => 'editUser']);
        Permission::create(['name' => 'editPermissions']);

        //Create Roles
        Role::create(['name' => 'superAdmin']);

        $waitingListAdminRole = Role::create(['name' => 'waitingListAdmin']);
        $waitingListAdminRole->givePermissionTo('viewWaitingList','editWaitingList','editUser');

        $waitingListViewerRole = Role::create(['name' => 'waitingListViewer']);
        $waitingListViewerRole->givePermissionTo('viewWaitingList');
    }
}
